CREATE TABLE IF NOT EXISTS combats (
  id INTEGER PRIMARY KEY,
  match_id INTEGER,
  pkm_id_user_1 INTEGER,
  pkm_id_user_2 INTEGER,
  pkm_id_winner INTEGER
);