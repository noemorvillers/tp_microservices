"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Type = void 0;
var Type;
(function (Type) {
    Type["STEEL"] = "steel";
    Type["COMBAT"] = "combat";
    Type["DRAGON"] = "dragon";
    Type["WATER"] = "water";
    Type["ELECTRIC"] = "electric";
    Type["FAIRY"] = "fairy";
    Type["FIRE"] = "fire";
    Type["ICE"] = "ice";
    Type["BUG"] = "bug";
    Type["NORMAL"] = "normal";
    Type["GRASS"] = "grass";
    Type["POISON"] = "poison";
    Type["PSYCHIC"] = "psychic";
    Type["ROCK"] = "rock";
    Type["GROUND"] = "ground";
    Type["SHADOW"] = "shadow";
    Type["DARK"] = "dark";
    Type["FLYING"] = "flying";
    Type["UNKNOWN"] = "unknown";
    Type["GHOST"] = "ghost";
})(Type = exports.Type || (exports.Type = {}));
//# sourceMappingURL=model.js.map