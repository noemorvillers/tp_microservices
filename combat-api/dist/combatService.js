"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.determineWinnerFromCombat = exports.getTypeFromId = exports.addCombat = exports.getAllCombats = void 0;
const combatRepository_1 = __importDefault(require("./combatRepository"));
const axios_1 = __importDefault(require("axios"));
const combatRepository = new combatRepository_1.default();
const getAllCombats = () => {
    return combatRepository.getAllCombats();
};
exports.getAllCombats = getAllCombats;
const addCombat = (matchId, pkmId_user1 = null, pkmnId_user2 = null, pkmId_winner = null) => {
    combatRepository.addCombat(matchId, pkmId_user1, pkmnId_user2, pkmId_winner);
    return true;
};
exports.addCombat = addCombat;
const getCombats = (id) => {
    return combatRepository.getCombatsFromIdMatch(id);
};
const determineWinnerFromCombat = (pkm_id_user_1, pkm_id_user_2) => __awaiter(void 0, void 0, void 0, function* () {
    let firstPokemonType = getTypeFromId(pkm_id_user_1).then(value => { return value; });
    let secondPokemonType = getTypeFromId(pkm_id_user_2).then(value => { return value; });
    let idPkmWinner = pkm_id_user_1;
    console.log(firstPokemonType);
    console.log(secondPokemonType);
    return idPkmWinner;
});
exports.determineWinnerFromCombat = determineWinnerFromCombat;
const addWinnerToCombat = (combat, pkmId_winner) => {
    combatRepository.addWinner(combat.id, pkmId_winner);
    return true;
};
/*
const determineAndSetWinnerFromCombat = (combat : Combat) : boolean => {
    let result : boolean = false;
    if(canStart(combat)){
        let idWinner : number = determineWinnerFromCombat(combat);
        addWinnerToCombat(combat, idWinner);
        result = true;
    }
    return result;
}
*/
const canStart = (combat) => {
    let result;
    if (combat.pkm_id_user_1 >= 0 && combat.pkm_id_user_2 >= 0) {
        result = true;
    }
    else {
        result = false;
    }
    return result;
};
const getScoreFromIdMatch = (matchId) => {
    let result;
    let combats = getCombatFromIdMatch(matchId);
    result[0] = 0;
    result[1] = 0;
    result[2] = 0;
    combats.forEach(function (combat) {
        let resultCombat = getUserWinnerFromCombat(combat);
        if (resultCombat == 1) {
            result[0]++;
        }
        else if (resultCombat == 2) {
            result[1]++;
        }
        else {
            result[2]++;
        }
    });
    return result;
};
const getUserWinnerFromCombat = (combat) => {
    let result = 0;
    if (combatGotWinner(combat)) {
        if (combat.pkm_id_winner == combat.pkm_id_user_1) {
            result = 1;
        }
        else if (combat.pkm_id_winner == combat.pkm_id_user_2) {
            result = 2;
        }
        else {
            result = -1;
        }
    }
    return result;
};
const combatGotWinner = (combat) => {
    return (combat.pkm_id_winner >= 0);
};
const getCombatFromIdMatch = (idMatch) => {
    return combatRepository.getCombatsFromIdMatch(idMatch);
};
const getTypeFromId = (idPokemon) => __awaiter(void 0, void 0, void 0, function* () {
    return axios_1.default.get('https://pokeapi.co/api/v2/pokemon/' + idPokemon + '/').then(res => res.data.types[0].type.name).then(res => { return res; });
});
exports.getTypeFromId = getTypeFromId;
//# sourceMappingURL=combatService.js.map