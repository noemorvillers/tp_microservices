"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const better_sqlite3_1 = __importDefault(require("better-sqlite3"));
const fs_1 = __importDefault(require("fs"));
class CombatRepository {
    constructor() {
        this.db = new better_sqlite3_1.default('db/combats.db', { verbose: console.log });
        this.applyMigrations();
    }
    applyMigrations() {
        const applyMigration = (path) => {
            const migration = fs_1.default.readFileSync(path, 'utf8');
            this.db.exec(migration);
        };
        const testRow = this.db.prepare("SELECT name FROM sqlite_schema WHERE type='table' AND name='combats'").get();
        if (!testRow) {
            console.log('Applying migrations on DB matches...');
            const migrations = ['db/migrations/init.sql'];
            migrations.forEach(applyMigration);
        }
    }
    getAllCombats() {
        const statement = this.db.prepare("SELECT * FROM combats");
        const rows = statement.all();
        return rows;
    }
    addCombat(matchId, pkmId_user1, pkmnId_user2, pkmId_winner = null) {
        const statement = this.db.prepare("INSERT INTO combats (match_id, pkm_id_user_1, pkm_id_user_2, pkm_id_winner) VALUES (?,?,?,?)");
        return statement.run(matchId, pkmId_user1, pkmnId_user2, pkmId_winner);
    }
    getCombatsFromIdMatch(matchId) {
        const statement = this.db.prepare("SELECT * FROM combats WHERE match_id = ?");
        const rows = statement.all(matchId);
        return rows;
    }
    addWinner(id, pkmId_winner) {
        const statement = this.db.prepare("INSERT INTO combats (pkm_id_winner) VALUES (?) WHERE id = ?");
        return statement.run(pkmId_winner, id);
    }
}
exports.default = CombatRepository;
//# sourceMappingURL=combatRepository.js.map