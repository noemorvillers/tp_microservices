"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getWinnerFromMatch = exports.returnType = void 0;
const CombatService = __importStar(require("./combatService"));
const returnType = (req, res) => {
    const pokemon_id = parseFloat(req.params.idPokemon);
    CombatService.getTypeFromId(pokemon_id).then(string => res.status(200).json(string));
};
exports.returnType = returnType;
const getWinnerFromMatch = (req, res) => {
    const pokemonId1 = parseFloat(req.params.idPokemon1);
    const pokemonId2 = parseFloat(req.params.idPokemon2);
    CombatService.determineWinnerFromCombat(pokemonId1, pokemonId2).then(winner => res.status(200).json(winner));
};
exports.getWinnerFromMatch = getWinnerFromMatch;
//# sourceMappingURL=combatController.js.map