export type Combat =
{
    id : number,
    pkm_id_user_1 : number,
    pkm_id_user_2 : number,
    pkm_id_winner : number
}

export enum Type
{
    STEEL = "steel",
    COMBAT = "combat",
    DRAGON = "dragon",
    WATER = "water",
    ELECTRIC = "electric",
    FAIRY = "fairy",
    FIRE = "fire",
    ICE = "ice",
    BUG = "bug",
    NORMAL = "normal",
    GRASS = "grass",
    POISON = "poison",
    PSYCHIC = "psychic",
    ROCK = "rock",
    GROUND = "ground",
    SHADOW = "shadow",
    DARK = "dark",
    FLYING = "flying",
    UNKNOWN = "unknown",
    GHOST = "ghost"
}

export type Pokemon = 
{
    pokemon_id : number;
    name : string;
    type : Type;
}
