import Database from 'better-sqlite3'
import { Combat } from './model';
import fs from 'fs'

export default class CombatRepository {
    db: Database.Database

    constructor() {
        this.db = new Database('db/combats.db', {verbose: console.log});
        this.applyMigrations();
    }

    applyMigrations(){
       const applyMigration = (path: string) => {
           const migration = fs.readFileSync(path, 'utf8')
           this.db.exec(migration)
       }

       const testRow = this.db.prepare("SELECT name FROM sqlite_schema WHERE type='table' AND name='combats'").get()
       if(!testRow)
       {
           console.log('Applying migrations on DB matches...')
           const migrations = ['db/migrations/init.sql']
           migrations.forEach(applyMigration)
       }
    }

    getAllCombats() : Combat[] {
        const statement = this.db.prepare("SELECT * FROM combats")
        const rows: Combat[] = statement.all()
        return rows
    }

    addCombat(matchId : number, pkmId_user1 : number, pkmnId_user2 : number, pkmId_winner : number = null) {
        const statement = this.db.prepare("INSERT INTO combats (match_id, pkm_id_user_1, pkm_id_user_2, pkm_id_winner) VALUES (?,?,?,?)")
        return statement.run(matchId, pkmId_user1, pkmnId_user2, pkmId_winner)
    }

    getCombatsFromIdMatch(matchId : number){
        const statement = this.db.prepare("SELECT * FROM combats WHERE match_id = ?")
        const rows: Combat[] = statement.all(matchId)
        return rows
    }

    addWinner(id : number, pkmId_winner : number){
        const statement = this.db.prepare("INSERT INTO combats (pkm_id_winner) VALUES (?) WHERE id = ?")
        return statement.run(pkmId_winner, id)
    }

}