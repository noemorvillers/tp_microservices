import * as CombatService from './combatService'
import * as express from "express"

const returnType = (req: express.Request, res: express.Response) => {
    const pokemon_id: number = parseFloat(req.params.idPokemon)
    CombatService.getTypeFromId(pokemon_id).then(string => res.status(200).json(string))
}

const getWinnerFromMatch = (req: express.Request, res : express.Response) => {
    const pokemonId1: number = parseFloat(req.params.idPokemon1)
    const pokemonId2: number = parseFloat(req.params.idPokemon2)
    CombatService.determineWinnerFromCombat(pokemonId1, pokemonId2).then(winner => res.status(200).json(winner))
}

export {returnType, getWinnerFromMatch}