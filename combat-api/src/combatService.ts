import CombatRepository from './combatRepository'
import {Combat, Type} from "./model"
import axios, { AxiosResponse } from 'axios'

const combatRepository = new CombatRepository();

const getAllCombats = () : Combat[] => {
	return combatRepository.getAllCombats();
}

const addCombat = (matchId : number, pkmId_user1 : number = null, pkmnId_user2 : number = null, pkmId_winner : number = null) : boolean => {
	combatRepository.addCombat(matchId, pkmId_user1, pkmnId_user2, pkmId_winner);
	return true;
}

const getCombats = (id : number) : Combat[] => {
    return combatRepository.getCombatsFromIdMatch(id)
}

const determineWinnerFromCombat = async(pkm_id_user_1 : number, pkm_id_user_2 : number) : Promise<Number> => {
    let firstPokemonType = getTypeFromId(pkm_id_user_1).then(value => {return value})
    let secondPokemonType = getTypeFromId(pkm_id_user_2).then(value => {return value})
    //Not fully implemented
    let idPkmWinner = pkm_id_user_1
    return idPkmWinner;
}

const addWinnerToCombat = (combat : Combat, pkmId_winner : number) : boolean => {
    combatRepository.addWinner(combat.id, pkmId_winner);
    return true;
}

/*
const determineAndSetWinnerFromCombat = (combat : Combat) : boolean => {
    let result : boolean = false;
    if(canStart(combat)){
        let idWinner : number = determineWinnerFromCombat(combat);
        addWinnerToCombat(combat, idWinner);
        result = true;
    }
    return result;
}
*/

const canStart = (combat : Combat) : boolean => {
    let result : boolean
    if(combat.pkm_id_user_1 >= 0 && combat.pkm_id_user_2 >= 0){
        result = true;
    } else {
        result = false;
    }
    return result;
}

const getScoreFromIdMatch = (matchId : number) : number[] => {
    let result : number[];
    let combats : Combat[] = getCombatFromIdMatch(matchId);
    result[0] = 0;
    result[1] = 0;
    result[2] = 0;

    combats.forEach(function(combat){
        let resultCombat : number = getUserWinnerFromCombat(combat);
        if(resultCombat == 1){
            result[0]++;
        } else if (resultCombat == 2){
            result[1]++;
        } else {
            result[2]++
        }
    })

    return result;
}

const getUserWinnerFromCombat = (combat : Combat) : number => {
    let result : number = 0;
    if(combatGotWinner(combat)){
        if (combat.pkm_id_winner == combat.pkm_id_user_1){
            result = 1;
        } else if (combat.pkm_id_winner == combat.pkm_id_user_2){
            result = 2;
        } else {
            result = -1;
        }
    }
    return result;
}

const combatGotWinner = (combat : Combat) : boolean => {
    return (combat.pkm_id_winner >= 0)
}

const getCombatFromIdMatch = (idMatch : number) : Combat[] => {
    return combatRepository.getCombatsFromIdMatch(idMatch);
}

const getTypeFromId = async (idPokemon : number) : Promise<Type> => 
{
    return axios.get('https://pokeapi.co/api/v2/pokemon/' + idPokemon + '/').then(res => res.data.types[0].type.name).then(res => {return (res as keyof typeof Type) as Type})
}

export {getAllCombats, addCombat, getTypeFromId, determineWinnerFromCombat}