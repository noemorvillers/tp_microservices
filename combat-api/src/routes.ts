import * as express from "express"
import * as CombatController from "./combatController"

export const register = ( app: express.Application ) => {

  app.get('/:id/fight/:idPokemon1/:idPokemon2', CombatController.getWinnerFromMatch)
 
  
}
