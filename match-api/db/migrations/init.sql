CREATE TABLE IF NOT EXISTS matches (
  id INTEGER PRIMARY KEY,
  user_id_1 INTEGER,
  user_id_2 INTEGER,
  launch_date DATE,
  winner_user_id INTEGER DEFAULT 0,
  current_state INTEGER DEFAULT 0
);