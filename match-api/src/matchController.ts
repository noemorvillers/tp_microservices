import {Match,User} from './model'
import * as MatchService from './matchService'
import * as express from "express"

const typedMatches: Match[] = []

const listMatches = (req : express.Request,  res : express.Response) => {
	const userId: number = parseFloat(req.params.id)

    const matches : Match[] = MatchService.getAllMatches()
    console.log(matches)

	res.status(200).json(matches)
}

const createMatch = (req : express.Request,  res : express.Response) => {

	const userId1: number = parseFloat(req.params.id)
    const user2: User = req.body
   
	res.status(200).json(MatchService.createMatch(userId1, user2.id))
}

export {listMatches, createMatch}