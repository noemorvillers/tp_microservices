import MatchRepository from './matchRepository'
import {Match} from "./model"

const matchRepository = new MatchRepository();

const getAllMatches = () : Match[] => {
	return matchRepository.getAllMatches();
}

const createMatch = (user1Id : number, userId2 : number) : boolean => {
	matchRepository.createMatch(user1Id, userId2);
	return true;
}

export {createMatch, getAllMatches}