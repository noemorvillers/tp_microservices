export type User =
{
    id : number;
    name : string;
    score : number;
    password : string;
    admin : boolean;
}


export type Match =
{
    id : number;
    user_1 : User;
    user_2 : User;
    deck_1 : Pokemon[];
    deck_2 : Pokemon[];
}

export enum Type
{
    STEEL = "steel",
    COMBAT = "combat",
    DRAGON = "dragon",
    WATER = "water",
    ELECTRIC = "electric",
    FAIRY = "fairy",
    FIRE = "fire",
    ICE = "ice",
    BUG = "bug",
    NORMAL = "normal",
    GRASS = "grass",
    POISON = "poison",
    PSYCHIC = "psychic",
    ROCK = "rock",
    GROUND = "ground",
    SHADOW = "shadow",
    DARK = "dark",
    FLYING = "flying",
    UNKNOWN = "unknown",
    GHOST = "ghost"
}

export type Pokemon = 
{
    pokemon_id : number;
    name : string;
    type : Type;
}

export enum MatchState
{
    WAITING = 0,
    REFUSED = 1,
    ONGOING = 2,
    OVER = 3
}

