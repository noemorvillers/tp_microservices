import * as express from "express"
import * as MatchController from "./matchController"
import { User } from './model'

export const register = ( app: express.Application ) => {
  app.get('/:id/matches', MatchController.listMatches)
  
  app.post('/:id/createMatch', MatchController.createMatch)
}
