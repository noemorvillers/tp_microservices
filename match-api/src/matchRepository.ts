import Database from 'better-sqlite3'
import { Match } from './model';
import fs from 'fs'

export default class MatchRepository {
    db: Database.Database

    constructor() {
        this.db = new Database('db/matches.db', {verbose: console.log});
        this.applyMigrations();
    }

    applyMigrations(){
       const applyMigration = (path: string) => {
           const migration = fs.readFileSync(path, 'utf8')
           this.db.exec(migration)
       }

       const testRow = this.db.prepare("SELECT name FROM sqlite_schema WHERE type='table' AND name='matches'").get()
       if(!testRow)
       {
           console.log('Applying migrations on DB matches...')
           const migrations = ['db/migrations/init.sql']
           migrations.forEach(applyMigration)
       }
    }

    getAllMatches() : Match[] {
        const statement = this.db.prepare("SELECT * FROM matches")
        const rows: Match[] = statement.all()
        return rows
    }

    createMatch(userId1 : number, userId2 : number) {
        const statement = this.db.prepare("INSERT INTO matches (user_id_1, user_id_2) VALUES (?,?)")
        return statement.run(userId1, userId2)
    }

}