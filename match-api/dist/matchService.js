"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getAllMatches = exports.createMatch = void 0;
const matchRepository_1 = __importDefault(require("./matchRepository"));
const matchRepository = new matchRepository_1.default();
const getAllMatches = () => {
    return matchRepository.getAllMatches();
};
exports.getAllMatches = getAllMatches;
const createMatch = (user1Id, userId2) => {
    matchRepository.createMatch(user1Id, userId2);
    return true;
};
exports.createMatch = createMatch;
//# sourceMappingURL=matchService.js.map