"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const better_sqlite3_1 = __importDefault(require("better-sqlite3"));
const fs_1 = __importDefault(require("fs"));
class MatchRepository {
    constructor() {
        this.db = new better_sqlite3_1.default('db/matches.db', { verbose: console.log });
        this.applyMigrations();
    }
    applyMigrations() {
        const applyMigration = (path) => {
            const migration = fs_1.default.readFileSync(path, 'utf8');
            this.db.exec(migration);
        };
        const testRow = this.db.prepare("SELECT name FROM sqlite_schema WHERE type='table' AND name='matches'").get();
        if (!testRow) {
            console.log('Applying migrations on DB matches...');
            const migrations = ['db/migrations/init.sql'];
            migrations.forEach(applyMigration);
        }
    }
    getAllMatches() {
        const statement = this.db.prepare("SELECT * FROM matches");
        const rows = statement.all();
        return rows;
    }
    createMatch(userId1, userId2) {
        const statement = this.db.prepare("INSERT INTO matches (user_id_1, user_id_2) VALUES (?,?)");
        return statement.run(userId1, userId2);
    }
}
exports.default = MatchRepository;
//# sourceMappingURL=matchRepository.js.map