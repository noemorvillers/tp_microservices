"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.MatchState = exports.Type = void 0;
var Type;
(function (Type) {
    Type["STEEL"] = "steel";
    Type["COMBAT"] = "combat";
    Type["DRAGON"] = "dragon";
    Type["WATER"] = "water";
    Type["ELECTRIC"] = "electric";
    Type["FAIRY"] = "fairy";
    Type["FIRE"] = "fire";
    Type["ICE"] = "ice";
    Type["BUG"] = "bug";
    Type["NORMAL"] = "normal";
    Type["GRASS"] = "grass";
    Type["POISON"] = "poison";
    Type["PSYCHIC"] = "psychic";
    Type["ROCK"] = "rock";
    Type["GROUND"] = "ground";
    Type["SHADOW"] = "shadow";
    Type["DARK"] = "dark";
    Type["FLYING"] = "flying";
    Type["UNKNOWN"] = "unknown";
    Type["GHOST"] = "ghost";
})(Type = exports.Type || (exports.Type = {}));
var MatchState;
(function (MatchState) {
    MatchState[MatchState["WAITING"] = 0] = "WAITING";
    MatchState[MatchState["REFUSED"] = 1] = "REFUSED";
    MatchState[MatchState["ONGOING"] = 2] = "ONGOING";
    MatchState[MatchState["OVER"] = 3] = "OVER";
})(MatchState = exports.MatchState || (exports.MatchState = {}));
//# sourceMappingURL=model.js.map