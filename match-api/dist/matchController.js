"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.createMatch = exports.listMatches = void 0;
const MatchService = __importStar(require("./matchService"));
const typedMatches = [];
const listMatches = (req, res) => {
    const userId = parseFloat(req.params.id);
    const matches = MatchService.getAllMatches();
    console.log(matches);
    res.status(200).json(matches);
};
exports.listMatches = listMatches;
const createMatch = (req, res) => {
    const userId1 = parseFloat(req.params.id);
    const user2 = req.body;
    res.status(200).json(MatchService.createMatch(userId1, user2.id));
};
exports.createMatch = createMatch;
//# sourceMappingURL=matchController.js.map